[CmdletBinding()]Param(
	[switch]$SkipNuGet,
	[switch]$Force
)

. $PSScriptRoot/00_header.ps1 -SkipNuGet:$SkipNuGet -Force:$Force

# $ROOTDIR = (Resolve-Path $PSScriptRoot/../).Path
$BASENAME = Split-Path -Path $PSCommandPath -Leaf
Write-Host -ForegroundColor Blue ">> $BASENAME"

Write-Host "`$host"
$host
Write-Host "`$host.ui.RawUI"
$host.ui.RawUI

Write-Host "Location"
Get-Location

Write-Host "Env:\"
Get-ChildItem Env:\ | Where-Object { $_.Name -like "CI_*" -or $_.Name -like "GITLAB_*" } | Format-Table Name, Value

Write-Host -ForegroundColor Blue "<< $BASENAME"
